/**
 * Automatically generated file. DO NOT MODIFY
 */
package com.chimou.flutter_hms_gms_availability;

public final class BuildConfig {
  public static final boolean DEBUG = Boolean.parseBoolean("true");
  public static final String LIBRARY_PACKAGE_NAME = "com.chimou.flutter_hms_gms_availability";
  public static final String BUILD_TYPE = "debug";
}
