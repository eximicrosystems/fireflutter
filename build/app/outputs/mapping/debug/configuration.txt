# The proguard configuration file for the following section is /Users/oscarsevilla/Documents/Workspaces/EXI/Testing/fireflutter/build/app/intermediates/proguard-files/proguard-android-optimize.txt-4.1.0
# This is a configuration file for ProGuard.
# http://proguard.sourceforge.net/index.html#manual/usage.html
#
# Starting with version 2.2 of the Android plugin for Gradle, this file is distributed together with
# the plugin and unpacked at build-time. The files in $ANDROID_HOME are no longer maintained and
# will be ignored by new version of the Android plugin for Gradle.

# Optimizations: If you don't want to optimize, use the proguard-android.txt configuration file
# instead of this one, which turns off the optimization flags.
# Adding optimization introduces certain risks, since for example not all optimizations performed by
# ProGuard works on all versions of Dalvik.  The following flags turn off various optimizations
# known to have issues, but the list may not be complete or up to date. (The "arithmetic"
# optimization can be used if you are only targeting Android 2.0 or later.)  Make sure you test
# thoroughly if you go this route.
-optimizations !code/simplification/arithmetic,!code/simplification/cast,!field/*,!class/merging/*
-optimizationpasses 5
-allowaccessmodification

-dontusemixedcaseclassnames
-dontskipnonpubliclibraryclasses
-verbose

# Preserve some attributes that may be required for reflection.
-keepattributes *Annotation*,Signature,InnerClasses,EnclosingMethod

-keep public class com.google.vending.licensing.ILicensingService
-keep public class com.android.vending.licensing.ILicensingService
-keep public class com.google.android.vending.licensing.ILicensingService
-dontnote com.android.vending.licensing.ILicensingService
-dontnote com.google.vending.licensing.ILicensingService
-dontnote com.google.android.vending.licensing.ILicensingService

# For native methods, see http://proguard.sourceforge.net/manual/examples.html#native
-keepclasseswithmembernames,includedescriptorclasses class * {
    native <methods>;
}

# Keep setters in Views so that animations can still work.
-keepclassmembers public class * extends android.view.View {
    void set*(***);
    *** get*();
}

# We want to keep methods in Activity that could be used in the XML attribute onClick.
-keepclassmembers class * extends android.app.Activity {
    public void *(android.view.View);
}

# For enumeration classes, see http://proguard.sourceforge.net/manual/examples.html#enumerations
-keepclassmembers enum * {
    public static **[] values();
    public static ** valueOf(java.lang.String);
}

-keepclassmembers class * implements android.os.Parcelable {
    public static final ** CREATOR;
}

# Preserve annotated Javascript interface methods.
-keepclassmembers class * {
    @android.webkit.JavascriptInterface <methods>;
}

# The support libraries contains references to newer platform versions.
# Don't warn about those in case this app is linking against an older
# platform version. We know about them, and they are safe.
-dontnote android.support.**
-dontnote androidx.**
-dontwarn android.support.**
-dontwarn androidx.**

# This class is deprecated, but remains for backward compatibility.
-dontwarn android.util.FloatMath

# Understand the @Keep support annotation.
-keep class android.support.annotation.Keep
-keep class androidx.annotation.Keep

-keep @android.support.annotation.Keep class * {*;}
-keep @androidx.annotation.Keep class * {*;}

-keepclasseswithmembers class * {
    @android.support.annotation.Keep <methods>;
}

-keepclasseswithmembers class * {
    @androidx.annotation.Keep <methods>;
}

-keepclasseswithmembers class * {
    @android.support.annotation.Keep <fields>;
}

-keepclasseswithmembers class * {
    @androidx.annotation.Keep <fields>;
}

-keepclasseswithmembers class * {
    @android.support.annotation.Keep <init>(...);
}

-keepclasseswithmembers class * {
    @androidx.annotation.Keep <init>(...);
}

# These classes are duplicated between android.jar and org.apache.http.legacy.jar.
-dontnote org.apache.http.**
-dontnote android.net.http.**

# These classes are duplicated between android.jar and core-lambda-stubs.jar.
-dontnote java.lang.invoke.**

# End of content from /Users/oscarsevilla/Documents/Workspaces/EXI/Testing/fireflutter/build/app/intermediates/proguard-files/proguard-android-optimize.txt-4.1.0
# The proguard configuration file for the following section is /Users/oscarsevilla/Documents/Workspaces/EXI/Testing/fireflutter/android/app/proguard-rules.pro
-ignorewarnings
-keepattributes *Annotation*
-keepattributes Exceptions
-keepattributes InnerClasses
-keepattributes Signature
-keep class com.hianalytics.android.**{*;}
-keep class com.huawei.updatesdk.**{*;}
-keep class com.huawei.hms.**{*;}

## Flutter wrapper
-keep class io.flutter.app.** { *; }
-keep class io.flutter.plugin.**  { *; }
-keep class io.flutter.util.**  { *; }
-keep class io.flutter.view.**  { *; }
-keep class io.flutter.**  { *; }
-keep class io.flutter.plugins.**  { *; }
-dontwarn io.flutter.embedding.**
-keep class com.huawei.hms.flutter.** { *; }
-repackageclasses

# End of content from /Users/oscarsevilla/Documents/Workspaces/EXI/Testing/fireflutter/android/app/proguard-rules.pro
# The proguard configuration file for the following section is /Users/oscarsevilla/Documents/Workspaces/EXI/Testing/fireflutter/build/app/intermediates/aapt_proguard_file/debug/aapt_rules.txt
-keep class androidx.core.app.CoreComponentFactory { <init>(); }
-keep class com.example.mobileservices.fireflutter.MainActivity { <init>(); }
-keep class com.google.android.gms.common.api.GoogleApiActivity { <init>(); }
-keep class com.huawei.agconnect.core.ServiceDiscovery { <init>(); }
-keep class com.huawei.agconnect.core.provider.AGConnectInitializeProvider { <init>(); }
-keep class com.huawei.hms.aaid.InitProvider { <init>(); }
-keep class com.huawei.hms.activity.BridgeActivity { <init>(); }
-keep class com.huawei.hms.activity.EnableServiceActivity { <init>(); }
-keep class com.huawei.hms.analytics.receiver.HiAnalyticsSvcEvtReceiver { <init>(); }
-keep class com.huawei.hms.hwid.internal.ui.activity.HwIdSignInHubActivity { <init>(); }
-keep class com.huawei.hms.update.provider.UpdateProvider { <init>(); }
-keep class com.huawei.updatesdk.fileprovider.UpdateSdkFileProvider { <init>(); }
-keep class com.huawei.updatesdk.service.otaupdate.AppUpdateActivity { <init>(); }
-keep class com.huawei.updatesdk.support.pm.PackageInstallerActivity { <init>(); }

# End of content from /Users/oscarsevilla/Documents/Workspaces/EXI/Testing/fireflutter/build/app/intermediates/aapt_proguard_file/debug/aapt_rules.txt
# The proguard configuration file for the following section is /Users/oscarsevilla/.gradle/caches/transforms-2/files-2.1/8e2a40e2f155703edab1db6c0a869a5b/jetified-agconnect-crash-1.4.2.301/proguard.txt
#
-keep class com.huawei.agconnect.crash.*{*;}
-keep class com.huawei.agconnect.crash.internal.bean.*{*;}
-keep class com.huawei.agconnect.crash.internal.AGConnectNativeCrash{*;}
-keep class com.huawei.agconnect.crash.internal.AGConnectNativeInfo{*;}
-keep class com.huawei.agconnect.crash.internal.log.UserMetadataManager{*;}
-keepclassmembers class ** {
    @com.huawei.agconnect.https.annotation.* <fields>;
}

-keepclassmembers class ** {
    @com.huawei.agconnect.datastore.annotation.* <fields>;
}
# End of content from /Users/oscarsevilla/.gradle/caches/transforms-2/files-2.1/8e2a40e2f155703edab1db6c0a869a5b/jetified-agconnect-crash-1.4.2.301/proguard.txt
# The proguard configuration file for the following section is /Users/oscarsevilla/.gradle/caches/transforms-2/files-2.1/59b1a98df0e22d6a8879e7eed98a4cc3/jetified-hianalytics-5.1.0.300/proguard.txt
-optimizationpasses 5
-dontusemixedcaseclassnames
-dontskipnonpubliclibraryclasses
-verbose
-optimizations !code/simplification/arithmetic,!field/*,!class/merging/*

-keep public class * extends android.app.Activity
-keep public class * extends android.app.Application
-keep public class * extends android.app.Service
-keep public class * extends android.content.BroadcastReceiver
-keep public class * extends android.content.ContentProvider
-keep public class * extends android.app.backup.BackupAgentHelper
-keep public class * extends android.preference.Preference
-keep public class com.android.vending.licensing.ILicensingService

-keep class com.huawei.hms.analytics.**{*;}

-keepclasseswithmembernames class * {
    native <methods>;
}

-keepclasseswithmembernames class * {
    public <init>(android.content.Context, android.util.AttributeSet);
}

-keepclasseswithmembernames class * {
    public <init>(android.content.Context, android.util.AttributeSet, int);
}
-keep class * implements android.os.Parcelable {
  public static final android.os.Parcelable$Creator *;
}
-keepclassmembers enum * {
    public static **[] values();
    public static ** valueOf(java.lang.String);
}

#openness
-keep interface com.huawei.hms.analytics.type.HAEventType{
    *;
}
-keep interface com.huawei.hms.analytics.type.HAParamType{
    *;
}
-keep class com.huawei.hms.analytics.HiAnalyticsTools{
        public static void enableLog();
        public static void enableLog(int);
}
-keep class com.huawei.hms.analytics.type.HAUserProfileType{
    *;
}

-keep class com.huawei.hms.analytics.HiAnalyticsInstance{
    *;
}

-keep class com.huawei.hms.analytics.HiAnalytics{
    *;
}
-keep enum com.huawei.hms.analytics.type.ReportPolicy{
    *;
}
-keep class com.huawei.hms.analytics.internal.filter.EventFilter{
    public void logFilteredEvent(java.lang.String, android.os.Bundle);
    public java.lang.String getUserProfile(java.lang.String);
}
# End of content from /Users/oscarsevilla/.gradle/caches/transforms-2/files-2.1/59b1a98df0e22d6a8879e7eed98a4cc3/jetified-hianalytics-5.1.0.300/proguard.txt
# The proguard configuration file for the following section is /Users/oscarsevilla/.gradle/caches/transforms-2/files-2.1/6805f8786e0474bbd219dfbd03390097/jetified-play-services-base-17.3.0/proguard.txt
# b/35135904 Ensure that proguard will not strip the mResultGuardian.
-keepclassmembers class com.google.android.gms.common.api.internal.BasePendingResult {
  com.google.android.gms.common.api.internal.BasePendingResult$ReleasableResultGuardian mResultGuardian;
}

# End of content from /Users/oscarsevilla/.gradle/caches/transforms-2/files-2.1/6805f8786e0474bbd219dfbd03390097/jetified-play-services-base-17.3.0/proguard.txt
# The proguard configuration file for the following section is /Users/oscarsevilla/.gradle/caches/transforms-2/files-2.1/b0d071430b020a6fc65b937a27b29187/jetified-play-services-basement-17.3.0/proguard.txt
# Proguard flags for consumers of the Google Play services SDK
# https://developers.google.com/android/guides/setup#add_google_play_services_to_your_project

# Keep SafeParcelable NULL value, needed for reflection by DowngradeableSafeParcel
-keepclassmembers public class com.google.android.gms.common.internal.safeparcel.SafeParcelable {
    public static final *** NULL;
}

# Needed for Parcelable/SafeParcelable classes & their creators to not get renamed, as they are
# found via reflection.
-keep class com.google.android.gms.common.internal.ReflectedParcelable
-keepnames class * implements com.google.android.gms.common.internal.ReflectedParcelable
-keepclassmembers class * implements android.os.Parcelable {
  public static final *** CREATOR;
}

# Keep the classes/members we need for client functionality.
-keep @interface android.support.annotation.Keep
-keep @android.support.annotation.Keep class *
-keepclasseswithmembers class * {
  @android.support.annotation.Keep <fields>;
}
-keepclasseswithmembers class * {
  @android.support.annotation.Keep <methods>;
}

# Keep androidX equivalent of above android.support to allow Jetification.
-keep @interface androidx.annotation.Keep
-keep @androidx.annotation.Keep class *
-keepclasseswithmembers class * {
  @androidx.annotation.Keep <fields>;
}
-keepclasseswithmembers class * {
  @androidx.annotation.Keep <methods>;
}

# Keep the names of classes/members we need for client functionality.
-keep @interface com.google.android.gms.common.annotation.KeepName
-keepnames @com.google.android.gms.common.annotation.KeepName class *
-keepclassmembernames class * {
  @com.google.android.gms.common.annotation.KeepName *;
}

# Keep Dynamite API entry points
-keep @interface com.google.android.gms.common.util.DynamiteApi
-keep @com.google.android.gms.common.util.DynamiteApi public class * {
  public <fields>;
  public <methods>;
}

# Needed when building against pre-Marshmallow SDK.
-dontwarn android.security.NetworkSecurityPolicy

# Needed when building against Marshmallow SDK.
-dontwarn android.app.Notification

# Protobuf has references not on the Android boot classpath
-dontwarn sun.misc.Unsafe
-dontwarn libcore.io.Memory

# Internal Google annotations for generating Proguard keep rules.
-dontwarn com.google.android.apps.common.proguard.UsedBy*

# Annotations referenced by the SDK but whose definitions are contained in
# non-required dependencies.
-dontwarn javax.annotation.**
-dontwarn org.checkerframework.**

# End of content from /Users/oscarsevilla/.gradle/caches/transforms-2/files-2.1/b0d071430b020a6fc65b937a27b29187/jetified-play-services-basement-17.3.0/proguard.txt
# The proguard configuration file for the following section is /Users/oscarsevilla/.gradle/caches/transforms-2/files-2.1/34db75befaa6ec8d51028819c427fe4a/core-1.2.0/proguard.txt
# aapt2 is not (yet) keeping FQCNs defined in the appComponentFactory <application> attribute
-keep class androidx.core.app.CoreComponentFactory

# End of content from /Users/oscarsevilla/.gradle/caches/transforms-2/files-2.1/34db75befaa6ec8d51028819c427fe4a/core-1.2.0/proguard.txt
# The proguard configuration file for the following section is /Users/oscarsevilla/.gradle/caches/transforms-2/files-2.1/68442f037efb0df2a2e11dcf2af210cf/lifecycle-runtime-2.2.0/proguard.txt
-keepattributes *Annotation*

-keepclassmembers enum androidx.lifecycle.Lifecycle$Event {
    <fields>;
}

-keep !interface * implements androidx.lifecycle.LifecycleObserver {
}

-keep class * implements androidx.lifecycle.GeneratedAdapter {
    <init>(...);
}

-keepclassmembers class ** {
    @androidx.lifecycle.OnLifecycleEvent *;
}

# this rule is need to work properly when app is compiled with api 28, see b/142778206
-keepclassmembers class androidx.lifecycle.ReportFragment$LifecycleCallbacks { *; }
# End of content from /Users/oscarsevilla/.gradle/caches/transforms-2/files-2.1/68442f037efb0df2a2e11dcf2af210cf/lifecycle-runtime-2.2.0/proguard.txt
# The proguard configuration file for the following section is /Users/oscarsevilla/.gradle/caches/transforms-2/files-2.1/b7298b321f06ee3610506246890dc3fa/jetified-savedstate-1.0.0/proguard.txt
# Copyright (C) 2019 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

-keepclassmembers,allowobfuscation class * implements androidx.savedstate.SavedStateRegistry$AutoRecreated {
    <init>();
}

# End of content from /Users/oscarsevilla/.gradle/caches/transforms-2/files-2.1/b7298b321f06ee3610506246890dc3fa/jetified-savedstate-1.0.0/proguard.txt
# The proguard configuration file for the following section is /Users/oscarsevilla/.gradle/caches/transforms-2/files-2.1/c1c55e17e470dc62903872493a4da61d/versionedparcelable-1.1.0/proguard.txt
-keep public class * implements androidx.versionedparcelable.VersionedParcelable
-keep public class android.support.**Parcelizer { *; }
-keep public class androidx.**Parcelizer { *; }
-keep public class androidx.versionedparcelable.ParcelImpl

# End of content from /Users/oscarsevilla/.gradle/caches/transforms-2/files-2.1/c1c55e17e470dc62903872493a4da61d/versionedparcelable-1.1.0/proguard.txt
# The proguard configuration file for the following section is /Users/oscarsevilla/.gradle/caches/transforms-2/files-2.1/acf2160506ca04072e8f08d1fc6a5dce/lifecycle-viewmodel-2.1.0/proguard.txt
-keepclassmembers,allowobfuscation class * extends androidx.lifecycle.ViewModel {
    <init>();
}

-keepclassmembers,allowobfuscation class * extends androidx.lifecycle.AndroidViewModel {
    <init>(android.app.Application);
}

# End of content from /Users/oscarsevilla/.gradle/caches/transforms-2/files-2.1/acf2160506ca04072e8f08d1fc6a5dce/lifecycle-viewmodel-2.1.0/proguard.txt
# The proguard configuration file for the following section is /Users/oscarsevilla/.gradle/caches/transforms-2/files-2.1/65c7a2ebc8fe45da4787b9c8a281a205/rules/lib/META-INF/proguard/androidx-annotations.pro
-keep,allowobfuscation @interface androidx.annotation.Keep
-keep @androidx.annotation.Keep class * {*;}

-keepclasseswithmembers class * {
    @androidx.annotation.Keep <methods>;
}

-keepclasseswithmembers class * {
    @androidx.annotation.Keep <fields>;
}

-keepclasseswithmembers class * {
    @androidx.annotation.Keep <init>(...);
}

# End of content from /Users/oscarsevilla/.gradle/caches/transforms-2/files-2.1/65c7a2ebc8fe45da4787b9c8a281a205/rules/lib/META-INF/proguard/androidx-annotations.pro
# The proguard configuration file for the following section is /Users/oscarsevilla/.gradle/caches/transforms-2/files-2.1/6bb1d4a6d83857e3f17438139527aafb/jetified-agconnect-credential-1.4.2.301/proguard.txt
#

-keep class com.huawei.agconnect.common.api.*{*;}
-keep class com.huawei.agconnect.credential.*{*;}

-keep class com.huawei.agconnect.credential.obs.*{*;}
# End of content from /Users/oscarsevilla/.gradle/caches/transforms-2/files-2.1/6bb1d4a6d83857e3f17438139527aafb/jetified-agconnect-credential-1.4.2.301/proguard.txt
# The proguard configuration file for the following section is /Users/oscarsevilla/.gradle/caches/transforms-2/files-2.1/0fc71c0a4fbeb8465d45e27b1ee57137/jetified-agconnect-https-1.4.2.301/proguard.txt
-keep class com.huawei.agconnect.https.annotation.** {*;}
# End of content from /Users/oscarsevilla/.gradle/caches/transforms-2/files-2.1/0fc71c0a4fbeb8465d45e27b1ee57137/jetified-agconnect-https-1.4.2.301/proguard.txt
# The proguard configuration file for the following section is /Users/oscarsevilla/.gradle/caches/transforms-2/files-2.1/8a6bd26b12bffcdc497f58b4aa61df92/jetified-base-5.1.0.300/proguard.txt
-keep class com.huawei.hms.common.** {*;}
-keep class com.huawei.hms.api.** {*;}
-keep class com.huawei.hms.support.** {*;}
-keep class com.huawei.hms.adapter.** {*;}
-keep class com.huawei.hms.core.** {*;}
-keep class com.huawei.hms.utils.** {*;}
-keep class com.huawei.hms.activity.** {*;}
-keep class com.huawei.hms.security.** {*;}
-keep class com.huawei.hms.actions.** {*;}
-keep class com.huawei.hms.base.** {*;}
# End of content from /Users/oscarsevilla/.gradle/caches/transforms-2/files-2.1/8a6bd26b12bffcdc497f58b4aa61df92/jetified-base-5.1.0.300/proguard.txt
# The proguard configuration file for the following section is /Users/oscarsevilla/.gradle/caches/transforms-2/files-2.1/dcd139356aa269643b77c3fec5164c1b/jetified-availableupdate-5.1.0.300/proguard.txt
-keep class com.huawei.updatesdk.** {*;}
-keep class com.huawei.hms.update.** {*;}
-keep class com.huawei.hms.adapter.** {*;}
-keep class com.huawei.hms.base.** {*;}
-keep class com.huawei.hms.availableupdate.** {*;}
# End of content from /Users/oscarsevilla/.gradle/caches/transforms-2/files-2.1/dcd139356aa269643b77c3fec5164c1b/jetified-availableupdate-5.1.0.300/proguard.txt
# The proguard configuration file for the following section is /Users/oscarsevilla/.gradle/caches/transforms-2/files-2.1/bfd4357e85ae6ed865f4084d1f9b67f4/jetified-device-5.1.0.300/proguard.txt
-keep class com.huawei.hms.utils.** {*;}
-keep class com.huawei.hms.common.** {*;}
-keep class com.huawei.hms.android.** {*;}
-keep class com.huawei.hms.support.** {*;}
-keep class com.huawei.hms.device.** {*;}
# End of content from /Users/oscarsevilla/.gradle/caches/transforms-2/files-2.1/bfd4357e85ae6ed865f4084d1f9b67f4/jetified-device-5.1.0.300/proguard.txt
# The proguard configuration file for the following section is /Users/oscarsevilla/.gradle/caches/transforms-2/files-2.1/7b4ac5d63e104ffc74410d07881081f3/jetified-agconnect-core-1.4.2.301/proguard.txt
-keep class * implements com.huawei.agconnect.core.ServiceRegistrar
# End of content from /Users/oscarsevilla/.gradle/caches/transforms-2/files-2.1/7b4ac5d63e104ffc74410d07881081f3/jetified-agconnect-core-1.4.2.301/proguard.txt
# The proguard configuration file for the following section is /Users/oscarsevilla/.gradle/caches/transforms-2/files-2.1/46b3a00eb7a84b15b106cf11155d9f09/jetified-datastore-core-1.4.2.301/proguard.txt
#
-keep class com.huawei.agconnect.datastore.**{*;}
# End of content from /Users/oscarsevilla/.gradle/caches/transforms-2/files-2.1/46b3a00eb7a84b15b106cf11155d9f09/jetified-datastore-core-1.4.2.301/proguard.txt
# The proguard configuration file for the following section is /Users/oscarsevilla/.gradle/caches/transforms-2/files-2.1/5bb420dd3f2dc8f396d2b56030d82f9a/rules/lib/META-INF/proguard/okhttp3.pro
# JSR 305 annotations are for embedding nullability information.
-dontwarn javax.annotation.**

# A resource is loaded with a relative path so the package of this class must be preserved.
-keepnames class okhttp3.internal.publicsuffix.PublicSuffixDatabase

# Animal Sniffer compileOnly dependency to ensure APIs are compatible with older versions of Java.
-dontwarn org.codehaus.mojo.animal_sniffer.*

# OkHttp platform used only on JVM and when Conscrypt dependency is available.
-dontwarn okhttp3.internal.platform.ConscryptPlatform

# End of content from /Users/oscarsevilla/.gradle/caches/transforms-2/files-2.1/5bb420dd3f2dc8f396d2b56030d82f9a/rules/lib/META-INF/proguard/okhttp3.pro
# The proguard configuration file for the following section is /Users/oscarsevilla/.gradle/caches/transforms-2/files-2.1/2fc604b96aee9a6605ac568bfdde9d3b/jetified-stats-5.1.0.300/proguard.txt
-keep class com.huawei.hms.utils.** {*;}
-keep class com.huawei.hms.support.** {*;}
-keep class com.huawei.hms.stats.** {*;}
# End of content from /Users/oscarsevilla/.gradle/caches/transforms-2/files-2.1/2fc604b96aee9a6605ac568bfdde9d3b/jetified-stats-5.1.0.300/proguard.txt
# The proguard configuration file for the following section is /Users/oscarsevilla/.gradle/caches/transforms-2/files-2.1/6edd6d0344e3497db4c122d54f60efe7/jetified-ui-5.1.0.300/proguard.txt
-keep class com.huawei.hms.ui.** {*;}
-keep class com.huawei.hms.activity.** {*;}
-keep class com.huawei.hms.utils.** {*;}
-keep class com.huawei.hms.base.** {*;}
-keep class com.huawei.hms.support.** {*;}
# End of content from /Users/oscarsevilla/.gradle/caches/transforms-2/files-2.1/6edd6d0344e3497db4c122d54f60efe7/jetified-ui-5.1.0.300/proguard.txt
# The proguard configuration file for the following section is /Users/oscarsevilla/.gradle/caches/transforms-2/files-2.1/9f36ab9be9bc423d2567bca48a5c2765/jetified-log-5.1.0.300/proguard.txt
-keep class com.huawei.hms.base.** {*;}
-keep class com.huawei.hms.support.** {*;}
-keep class com.huawei.hms.log.** {*;}
# End of content from /Users/oscarsevilla/.gradle/caches/transforms-2/files-2.1/9f36ab9be9bc423d2567bca48a5c2765/jetified-log-5.1.0.300/proguard.txt
# The proguard configuration file for the following section is /Users/oscarsevilla/.gradle/caches/transforms-2/files-2.1/f5a8aaf3cda902428216476cb55313d8/jetified-network-grs-4.0.20.301/proguard.txt
-keep class com.huawei.hms.framework.network.grs.GrsApi {   *; }
-keep class com.huawei.hms.framework.network.grs.GrsBaseInfo {   *; }
-keep class com.huawei.hms.framework.network.grs.GrsBaseInfo$* {*;}
-keep class com.huawei.hms.framework.network.grs.IQueryUrlCallBack {   *; }
-keep class com.huawei.hms.framework.network.grs.IQueryUrlsCallBack {   *; }
-keep class com.huawei.hms.framework.network.grs.GrsClient {   *; }
-keep class com.huawei.hms.framework.network.grs.local.model.CountryCodeBean {   *; }
-keep class com.huawei.hms.framework.network.grs.GrsManager {   *; }
-keep class com.huawei.hms.framework.network.grs.GrsConfig {   *; }
-keep class com.huawei.hms.framework.network.grs.GrsApp {   *; }

# End of content from /Users/oscarsevilla/.gradle/caches/transforms-2/files-2.1/f5a8aaf3cda902428216476cb55313d8/jetified-network-grs-4.0.20.301/proguard.txt
# The proguard configuration file for the following section is /Users/oscarsevilla/.gradle/caches/transforms-2/files-2.1/346ddb27753fcadafb9b68816fefb2f5/jetified-hatool-5.1.0.300/proguard.txt
-keep class com.huawei.hianalytics.** {*;}
# End of content from /Users/oscarsevilla/.gradle/caches/transforms-2/files-2.1/346ddb27753fcadafb9b68816fefb2f5/jetified-hatool-5.1.0.300/proguard.txt
# The proguard configuration file for the following section is /Users/oscarsevilla/.gradle/caches/transforms-2/files-2.1/f1a90a95715d88ebadee310b1dd208fd/jetified-network-common-4.0.20.301/proguard.txt
# Add project specific ProGuard rules here.
# You can control the set of applied configuration files using the
# proguardFiles setting in build.gradle.
#
# For more details, see
#   http://developer.android.com/guide/developing/tools/proguard.html

# If your project uses WebView with JS, uncomment the following
# and specify the fully qualified class name to the JavaScript interface
# class:
#-keepclassmembers class fqcn.of.javascript.interface.for.webview {
#   public *;
#}

# Uncomment this to preserve the line number information for
# debugging stack traces.
#-keepattributes SourceFile,LineNumberTable

# If you keep the line number information, uncomment this to
# hide the original source file name.
#-renamesourcefileattribute SourceFile

-keep class com.huawei.hms.framework.common.** {   *; }
# End of content from /Users/oscarsevilla/.gradle/caches/transforms-2/files-2.1/f1a90a95715d88ebadee310b1dd208fd/jetified-network-common-4.0.20.301/proguard.txt
# The proguard configuration file for the following section is <unknown>
-ignorewarnings
# End of content from <unknown>