class AnalyticsParams {
  static const String ITEM_CATEGORY = 'item_category';
  static const String ITEM_NAME = 'item_name';
}

class AnalyticsEvent {
  static const String SELECT_CONTENT = 'select_content';
}

class AnalyticsUserParams {
  static const String ITEM_IDSEGUIMIENTO = 'item_seguimiento';
  static const String ITEM_TIPO = 'item_tipo';
  static const String ITEM_EMPRESA = 'item_empresa';
  static const String ITEM_NACIMIENTO = 'item_nacimiento';
  static const String ITEM_CP = 'item_cp';
  static const String ITEM_GENERO = 'item_genero';
  static const String ITEM_RIESGO = 'item_riesgo';
}