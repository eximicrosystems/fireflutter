import 'package:fireflutter/mobileServices/HuaweiMobileService.dart';
import 'package:fireflutter/mobileServices/GoogleMobileService.dart';
import 'package:fireflutter/mobileServices/MobileService.dart';
import 'package:flutter/material.dart';
import 'package:flutter_hms_gms_availability/flutter_hms_gms_availability.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Mobile Service Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(title: 'Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);
  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  MobileService services = MobileService();

  @override
  void initState() {
    _checkMS();
    super.initState();
  }
  void _checkMS() {
    FlutterHmsGmsAvailability.isHmsAvailable.then((t) {
      if(t){
        print('HMS Available');
        services = HuaweiMobileService();
        services.initAnalytics();
        services.setEvent("Home Page");
      }else{
        print('GMS Available');
        services = GoogleMobileService();
        services.initAnalytics();
      }
    });
  }
  void _setEvent(){
    services.setEvent("Home Page");
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(
              'Prueba de',
            ),
            Text(
              'Servicios Moviles',
              style: Theme.of(context).textTheme.headline4,
            ),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: _setEvent,
        tooltip: 'Increment',
        child: Icon(Icons.add),
      ), // This trailing comma makes auto-formatting nicer for build methods.
    );
  }
}
